package entity;

import entity.TTask;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-22T23:34:37")
@StaticMetamodel(TProject.class)
public class TProject_ { 

    public static volatile ListAttribute<TProject, TTask> task;
    public static volatile SingularAttribute<TProject, String> name;
    public static volatile SingularAttribute<TProject, Date> term;
    public static volatile SingularAttribute<TProject, Long> id;
    public static volatile SingularAttribute<TProject, String> category;
    public static volatile SingularAttribute<TProject, String> priority;

}