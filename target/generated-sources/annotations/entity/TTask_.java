package entity;

import entity.TEmployee;
import entity.TProject;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-22T23:34:37")
@StaticMetamodel(TTask.class)
public class TTask_ { 

    public static volatile SingularAttribute<TTask, TProject> project_id;
    public static volatile SingularAttribute<TTask, Date> endDate;
    public static volatile ListAttribute<TTask, TEmployee> employee_id;
    public static volatile SingularAttribute<TTask, String> taskName;
    public static volatile SingularAttribute<TTask, Long> id;
    public static volatile SingularAttribute<TTask, Date> startDate;
    public static volatile SingularAttribute<TTask, String> status;

}