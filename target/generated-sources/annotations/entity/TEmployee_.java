package entity;

import entity.TTask;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-22T23:34:37")
@StaticMetamodel(TEmployee.class)
public class TEmployee_ { 

    public static volatile SingularAttribute<TEmployee, Integer> reward;
    public static volatile SingularAttribute<TEmployee, String> firstName;
    public static volatile SingularAttribute<TEmployee, String> lastName;
    public static volatile SingularAttribute<TEmployee, String> password;
    public static volatile ListAttribute<TEmployee, TTask> task;
    public static volatile SingularAttribute<TEmployee, Integer> permissions;
    public static volatile SingularAttribute<TEmployee, Long> id;
    public static volatile SingularAttribute<TEmployee, String> email;

}